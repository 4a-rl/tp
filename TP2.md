
# Utilisation de Python avec Mininet

L'utilisation interactive de Mininet permet d'en comprendre plus ou moins les mécanismes qui sont utilisés. Cependant, il ne permet pas d'automatiser les tests. Mininet fournit une api  qui permet d'utiliser le langage python pour faciliter la création de réseau/topologie et les tests. 

Dans ce TP, nous allons utiliser un script python pour lancer l'émulation. (création de la topologie et lancement de iperf). 

### Premier exemple
L'exemple suivant, permet d'effectuer la même émulation que dans le [TP1](https://gitlab.com/4A-RL/tp/TP1.md).

```python
#!/usr/bin/python

"""
This example shows how to create an empty Mininet object
(without a topology object) and add nodes to it manually.
"""

from mininet.net import Mininet
from mininet.node import Controller
from mininet.cli import CLI
from mininet.log import setLogLevel, info

def myNet():

    "Create an empty network and add nodes to it."
    net = Mininet( controller=Controller )

    "Ajout d'un controleur"
    net.addController( 'c0' )

    "Ajout des hôtes"
    h1 = net.addHost( 'h1', ip='10.0.0.1' )
    h2 = net.addHost( 'h2', ip='10.0.0.2' )

    "Ajout du switch"
    s3 = net.addSwitch( 's3' )

    "Creation des liens"
    net.addLink( h1, s3 )
    net.addLink( h2, s3 )

    "Démarrage du réseau"
    net.start()

#  Options client de iperf:
# -i: interval between reports set to 1sec
# -l: length read/write buffer set to default 8KB
# -w: TCP window size (socket buffer size) set to 16MB
# -M: TCP MSS (MTU-40B) set to 1460B for an MTU of 1500B
# -N: disable Nagle's Alg
# -Z: select TCP Congestion Control alg
# -t: transmission time
# -f: format set to kilobits
# -y: report style set to CSV
# Le résultat est rediriger vers res.raw au format CSV.

    "Lancement de iperf"
    info( '*** Starting iperf\n' )
    srv = h2.popen(['iperf', '-s', '-p', '5560'])
    cli = h1.popen('iperf -c 10.0.0.2 -p 5560 -i 1 -t 15 -y C > res.raw', shell=True)
    
    " Attente que le client ai fini... 15s"
    cli.wait()
    
   "Arret du serveur"
    srv.terminate()
    
    "Remise en attente du serveur... au cas ou..."
    srv.wait()
    
    "Si on veut le prompt mininet... "
    #CLI( net )

    "Arret de l'emulation"
    net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )
    myNet()
```

Une fois le code python précédent enregistrer dans un fichier `exo1.py` par exemple, il suffit de le lancer
```bash
$ python2 exo1.py
```

Le fichier `res.raw` crée dans le répertoire de lancement sera de la forme (avec l'option -y C dans iperf): 
```
20201112053441,10.0.0.1,41618,10.0.0.2,5560,13,0.0-1.0,2135687168,17085497344
20201112053442,10.0.0.1,41618,10.0.0.2,5560,13,1.0-2.0,4327997440,34623979520
20201112053443,10.0.0.1,41618,10.0.0.2,5560,13,2.0-3.0,4469686272,35757490176
20201112053444,10.0.0.1,41618,10.0.0.2,5560,13,3.0-4.0,4426956800,35415654400
20201112053445,10.0.0.1,41618,10.0.0.2,5560,13,4.0-5.0,4597219328,36777754624
20201112053446,10.0.0.1,41618,10.0.0.2,5560,13,5.0-6.0,4371382272,34971058176
20201112053447,10.0.0.1,41618,10.0.0.2,5560,13,6.0-7.0,4442030080,35536240640
20201112053448,10.0.0.1,41618,10.0.0.2,5560,13,7.0-8.0,4522901504,36183212032
20201112053449,10.0.0.1,41618,10.0.0.2,5560,13,8.0-9.0,4450680832,35605446656
20201112053450,10.0.0.1,41618,10.0.0.2,5560,13,9.0-10.0,4366270464,34930163712
```

L'avant dernière colonne représente le nombre d'octet transférer et la dernière colonne le débit par seconde. 

EXO1 :
> Dans le même fichier `exo1.py` mettre le traitement permettant de générer le graphe de l'evolution du débit.
> Pour cela utiliser par exemple matplotlib. 


### Modification de la qualité du lien
Il est possible de modifier la qualité des liens dans mininet. 
Pour cela, il faut importer le module link de mininet `from mininet.link import TCLink`
Après cela, il est ensuite possible de modifier la qualité des liens, la bande passante etc...

```
net.addLink( h1, s3, cls=TCLink, bw=3, delay='2ms', max_queue_size=80*delay, loss=10)
```
- bw donne la bande passante en Mbps
- delay donne le delais en ms. 
- max_queue_size donne la taille de la file, ici 2x80. 
- L'option loss permet de mettre un taux de perte sur les liens. Il est donné en pourcentage.
- Attention, le paramètre loss allonge le temps d'emulation.
- Plus de paramètres [ici](http://mininet.org/api/classmininet_1_1link_1_1TCIntf.html)

EXO2 :
> Dans un fichier `exo2.py` changer les paramètres des liens pour voir l'effet sur les débit obtenus.
> Utiliser iperf avec udp et tcp pour comparer les débits obtenus en variant les délais et les tailles de files d'attentes
>
> Delais variants de : 10 à 100 ms par pas de 10ms
> 
> Taille de la file d'attente variant de 10 à 80 (multiplicateur) par pas de 10. 

### Flux concurents
Nous allons utiliser la commande  `sleep`` de linux pour permettre de décaller le demarrage de plusieurs flux provenant d'un même hote. Pour cela il faut 
importer:  `from time import sleep, mktime` dans votre script python. 
```python
    srv0 = h2.popen(['iperf', '-s', '-p', '5560'])
    srv1 = h2.popen(['iperf', '-s', '-p', '5561'])

    cli0 = h1.popen('iperf -c 10.0.0.2 -p 5560 -i 1 -t 15 -y C > res0.raw', shell=True)
    " Attente de 5 secondes
    sleep(5)
    " départ du flux 2, ne pas oublier de changer le port... et de réduire le si besoin.. je change le fichier de sortie aussi
    cli1 = h1.popen('iperf -c 10.0.0.2 -p 5561 -i 1 -t 10 -y C > res1.raw', shell=True)

    cli0.wait()
    cli1.wait()

    srv0.terminate()
    srv1.terminate()
    
    srv0.wait()
    srv1.wait()
```

EXO3:
> Mettre en place la concurence entre deux flux TCP

EXO4:
> Mettre en place la concurence entre un flux TCP et un flux UDP
> Changer l'ordre de départ des flux, par exemple pour un flux udp
> ```python
> srv0 = h2.popen(['iperf', '-s', '-p',  '-u', '5560'])
> cli0 = h1.popen('iperf -c 10.0.0.2 -p 5560 -u -b 3M -i 1 -t 15 -y C > res0.raw', shell=True)
> ```
> Faites varier l'optionn -b du flux udp de 1 à 3 par pas de 0.2


