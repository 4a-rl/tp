# Introduction à MININET

## Introduction
[MININET](http://mininet.org/) est un émulateur/simulateur de réseaux. Comparé à NS-2 ou NS-3, Mininet utilise le noyaux linux et le code qui lui est associé. Par exemple, le code TCP utilisé dans mininet et le code du noyau linux alors que le code utilisé dans NS-2 ou NS-3 est du code produit par les développeurs de NS-2 ou NS-3. MININET utilise le code qui tourne dans la plupart des sytèmes actuellement déployés.

L'utilisation du code "réel" présente des avantages et des inconvénients. Les performances obtenus avec MININET sont ou seront les performances réelles de votre systèmes.  L'utilisation du code réel rend difficile le test de nouvelle variante d'un protocole qui ne serait pas encore implémenté dans le noyau. Par exemple si vous voulez créer une version de TCP qui vous est propre il faudra code cette variante dans le noyau linux. MININET est avantageux pour l'evaluation de performance de système réel utilisant des outils existants. 

Mininet est une solution logicielle, qui permet de mettre en place simplement des topologies réseau au plus proche des besoins de vos tests. Mininet vous permet d'utiliser les outils linux de base pour tester les performances des protocoles réseaux ou des applications sur cette topologie réseaux. Mininet vous permet de déployer, sur une seule machine, des réseaux, commutateurs, routeurs, etc. ainsi que vos applications et outils dans des "hôtes virtuels". 

## Installation
Vous pouvez installer mininet en suivant les indications qui se trouvent [ici](http://mininet.org/download/).  Je vous mets à disposition [ici](https://drive.google.com/file/d/1jDpGdRQ8ifMFofEguM6mFY0QXdV-2pm2/view?usp=sharing) une machine virtuelle devuan contenant une installation de mininet. Le login mot de passe pour cette machine est: **root/toor**. Pour l'interface graphique tapez : **startx**.


Notez que dans certain système, Mininet est disponible en package c'est le cas de ubuntu ou debian et peut s'installer avec la commande suivante.
```
apt-get install mininet
```


## Premier contact avec Mininet

Pour lancer mininet
```
$ sudo mn
```
Cette commande démarre mininet avec une topologie basique comportant deux hôtes (h1 et h2) et un switch (s1). Par convention le nom des hôtes commencent par **h** et le celui des switch par **s**.  La topologie utilisé par défaut par mininet est donné dans la figure suivante:


![diagramme](https://drive.google.com/uc?id=15L8fdJGtjwz13QhtyNMGZMPeEfX6nhEm)

Après le lancement de la commande précédente on se retrouve sur le prompt mininet
```
mininet > 
```

1. Pour voir les commandes que vous pouvez utiliser dans ce prompt :
```
mininet > help
```
2. Pour voir les noeuds disponibles dans le réseaux
```
mininet > nodes
available nodes are:
h1 h2 s1
mininet >
```
3.  La commande `net` permet d'afficher les lien disponibles
```
mininet > net
h1 h1-eth0:s1-eth1 
h2 h2-eth0:s1-eth2
s1 lo s1-eth1:h1-eth0 s1-eth2:h2-eth0
mininet >
```
Le résultat de la commande précédente montre que `h1` utilise l'interface `h1-eth0` qui est connecté à l'interface `s1-eth1`sur switch `s1`. De même pour `s2`. 

4. Vous pouvez utiliser/executer les commandes linux de base et d'autre. Par exemple
```
mininet > h1 ifconfig
```
Cette commande execute la commande ifconfig sur l'hôte `h1`. Vous pouvez constater que l'hôte à deux interfaces `lo` et `h1-eth0` et leur configuration. Notons que `h1` utilise un noyau linux avec une réplication de votre système linux actuel. 

5. Vous pouvez ainsi tester la connectivité de votre réseau
```
mininet > h1 ping 10.0.0.2
```
`ctrl+c` pour arrêter le ping.

6. Vous pouvez aussi lancer un terminal pour faciliter les interactions. Par exemple pour lancer un xterm sur les hôtes `h1`et `h2`.  
 ```
mininet > h1 xterm &
mininet > h2 xterm &
```

7. pour arrêter l'émulation vous pouvez taper `exit`. Passez à la section suivante pour faire vos premiers tests.
```
mininet > exit
```
La commande suivant permet de faire une purge de mininet. 
```
$ mn -c
```

## Evaluation basique
Il existe plusieurs générateurs de trafic sous linux [iperf](https://iperf.fr/) ou [mgen](https://www.nrl.navy.mil/itd/ncs/products/mgen). Il y en a d'autre  que je vous laisserai découvrir. Dans cette partie du TP, nous allons essayer la commande **iperf**. Je vous invite a consulter la documentation sur cette commande. 

Nous supposons ici que vous avez lancer un xterm pour **h1** et pour **h2**. Pour faciliter la lecture, le prompt sera changé.

Il faut tout d'abord lancer le serveur sur **h2** lancer la commande ci-dessous. Cette commande permet de lancer un serveur TCP (option **-s** ) sur le port 5566 (**-p 5566**) avec un monitoring interactif toutes les secondes (**-i 1**). 
```
root@h2$ iperf -s -p 5566 -i 1  
```

Ensuite, sur l'hôte **h1** nous lançons un client TCP avec la commande ci-dessous. Cette commande permet de lancer un client TCP (option **-c 10.0.0.2**) qui se connectera au serveur **10.0.0.2** (adresse de **h2**) sur le port 5566 (**-p 5566**) pendant 15 secondes (**-t 15**). Sur le client aussi il est possible d'avoir un affichage par seconde avec l'option **-i**.  
```
root@h1$ iperf -c 10.0.0.1 -p 5566 -t 15 -i 1 
```

#### EXO 1: 

> Tracer un graphique de l'évolution du débit par seconde sur ce flux TCP. Vous pouvez rediriger la sortie sur le serveur dans un fichier. `root@h2$ iperf -s -p 5566 -i 1 > result.dat`. Vous pouvez utiliser [gnuplot](www.gnuplot.info) ou [matplotlib](https://matplotlib.org/).

#### EXO 2: 

> Utiliser **iperf** pour générer un flux UDP et tracer le graphe. Que signifie l'option **-b**
> ` root@h2 $ iperf -s -u -p 5566 -i 1`
> ` root@h1 $ iperf -c 10.0.0.2 -u -b 10M -p 5566 -t 15`
 > De même tracer les résultats sur une courbe. 

#### EXO 3: 
> Faire la même expérimentation avec la commande mgen. La forme d'une commande mgen est la suivante : 
> `mgen input <scriptfile> [output<logfile>]`
> Le script pour l'envoi sera de la forme
> ```
> # Demarrer (instant 0.0) un flux numero 1 MGEN envoyant des messages UDP
> # sur le port 5566 de l'adresse IP 10.0.0.2
> # a un debit moyen de 10.0 octets par seconde,selon une loi de Poisson avec des messages de 1024 octets
> 0.0 ON 1 UDP DST 10.0.0.2/5566 POISSON [10.0 1024]
> 500.0 OFF 1
> ``` 
> Le script pour la réception sera de la forme
> ```
> # fichier recv.mgen
> # Ecoute sur les ports 5566, 5567 d'un flux UDP
> 0.0 LISTEN UDP 5566,5567
> ```
> `mgen input recv.mgen > /tmp/rawdata.mgen`
> 
> L'objectif est de realiser des essais permettant de vous familiarise ravec l'outil et de savoir fabriquer des profils de trafic qui permettront de servir pour observer les effets d'un contrôle de trafic dans les TP suivants.
> Testez différents types de trafic et de profil de trafic. Vous pouvez utiliser l'outis `trpr` (il faut installer l'outils) pour unn traçage en "temps réel".  A l'origine, `trpr` est prévu pour être utilisé avec `tcpdump` mais il est possible de l'utiliser avec les sorties de `mgen`. 
> `mgen -input rawdata.mgen | trpr mgen real auto X | gnuplot -noraise -persist`

Vous pouvez maintenant décider si vous préférer utiliser `mgen` ou `iperf` ou un autre générateur de trafic, dans tous les cas plus vous saurez en utiliser, mieux ce sera.



## NOTES sur la génération de trafic


### Les outils
Pour génerer du trafic :
    - UDP
    - TCP

Vous pouvez utiliser les outils [mgen](http://downloads.pf.itd.nrl.navy.mil/docs/mgen/mgen.html) pour générer du  trafic et [trpr](http://downloads.pf.itd.nrl.navy.mil/docs/proteantools/trpr.html) pour visionner.

Il faut e pense installer les outils suivants si ce n'est pas déjà fait:
```
sudo apt-get install gnuplot wireshark libpcap-dev iproute siege vlc vsftpd mini-httpd mgen trpr
```
`iproute` contient les outils tels que `ip` et `tc`.



L'outil [trpr](http://downloads.pf.itd.nrl.navy.mil/docs/proteantools/trpr.html) doit vous permettre d'obtenir les mesures de QoS quand vous utilisez [mgen](http://downloads.pf.itd.nrl.navy.mil/docs/mgen/mgen.html)

Pour info. `mgen` se lance par la commande : `mgen input <scriptfile> [output<logfile>]` le fichier scriptfile est un fichier qui comporte les indications sur la forme du trafic le temps, les ports et adresses IP de destination.

Un exemple de script mgen d'émission:
```
#Démarrer (instant0.0) un flux numéro 1 MGEN envoyant des messages UDP de 1024 octets 
#sur le port 5000 de l'adresse IP locale (machine locale)
#a un débit moyen de 10.0 octets par seconde, selon une loi de Poisson 
#L'emission du flux numéro 1 s'arrete au temps 500.0
0.0 ON 1 UDP DST 127.0.0.1/5000 POISSON [10.0 1024]
500.0 OFF 1
```

Vous pouvez générer plusieurs type de trafic, je veux dire autre que poisson.
Testez par exemple PERIODIC.

Un exemple de script mgen d'émission:
```
0.0 LISTEN UDP 5000
```

Récupérer les données de réception dans un fichier. En supposant que votre 
script de réception s'appelle `recv.mgen`, on redirige la sortie vers un fichier
`/tmp/rawdata`

```
mgen input recv.mgen > /tmp/rawdata.mgen
```

On traite ce fichier avec `trpr`. Les commandes suivantes fcontionnent:

```
trpr input /tmp/rawdata.mgen history 500 window 1 mgen real > debit.raw
```

ici, history permet de spécifier le temps (axe des x) et window permet de spécifier les points sur cet axe des x, ici toutes les seconds (je crois). On redirige la sortie vers `debit.dat`. Vous avez les options suivantes pour la commande précédente:

```
trpr input rawdata history 500 window 1 mgen real loss > loss.raw
trpr input rawdata history 500 window 1 mgen real interarrival > interarrival.raw
```

Normalement, les fichiers `*.raw` générer doivent pouvoir être traités directement avec gnuplot mais je n'ai pas réussi. Il faut éditer le fichier en 2 fichiers.

NOTE: la deuxième commande permet d'avoir les temps inter-arrivé des paquets. Pour avoir la gigue il faut faire un petit calcul. 



 
Un fichier `debit.gnu` qui contient:
```
set terminal pngcairo size 350,262 enhanced font 'Verdana,10'
set output 'debit.png'
set xlabel 'Time (sec)'
set ylabel 'Rate (kbps)'
set style data lines
set yrange[0.000000:*]
set key bottom right
set xrange[0.000000:120.000000]
plot 'debit.dat' w lp t 'mgen,1 @IP SRC->@IP SRC/port~1'
```

A quelques détails près ces lignes sont dans votre fichier `*.raw`

et un Fichier `debit.dat`

```
0.000000, 8306.688000
1.000000, 8306.688000
2.000000, 8085.504000
...
...
120.000000, 8005.504000
```

La commande 
```
$ gnuplot debit.gnu
``` 
devrait vous générer un fichier `debit.png` qui contient un plus ou moins joli graphe. Consultez les pages de [gnuplot](http://www.gnuplot.info/) pour avoir des graphes vraiment jolis avec un aspect un peu plus professionnel.

Vous pouvez automatiser tout ce processus... avec un gros script bash/awk/python comme vous voulez. Faîtes-le vous gagnerez du temps. Enfin je crois.

Je vous recommande d'utiliser `tcpdump` pour capturer le trafic générer par `mgen` et d'analyser ce trafic avec `trpr`. 


### TEST 1: Test et génération de trafic HTTP

Installer un serveur http comme [mini-httpd](http://manpages.ubuntu.com/manpages/natty/man8/mini-httpd.8.html) par exemple.

Pour stresser votre serveur http, utiliser l'outil [siege](https://www.joedog.org/siege-manual/). Cet outil est une commande qui génère des requêtes http à un serveur. L'intérêt  de cet outil est qu'il permet de générer ces requêtes de façon concurrente (i.e. en parallèle) et donc de « stresser » votre serveur.

Le trafic HTTP va surtout nous servir de trafic de background. Il n'est pas important de  recueillir les informations de performance sur ce trafic. (enfin je crois)

Dans un répertoire du serveur, créez un fichier `index.html` plus ou moins gros avec plus  ou moins d'images. Comme vous voulez en fait. Sur le serveur lancez (et dans ce répertoire):

```
sudo mini-httpd
```

Côté client, lancé un navigateur pour tester si tout fonctionne. Normalement, 
les pages sont servies sur le port 80. `mini-httpd -p 8080` pour les servir
sur le port 8080. Voir page du manuel de `mini-httpd` car je ne suis pas certain.

```
 siege -c 800 http://@IPServeur:8080
```

cette commande simule 800 requêtes simultanées, attends une seconde, puis relance 800 requêtes et ainsi de suite. Laissez tourner le programme quelques secondes (20 tout au plus) et arrêtez le à l'aide de Ctrl+C.

Utiliser [`tcpdump`](http://www.tcpdump.org/tcpdump_man.html) avec l'option `-X` pour capturer le trafic et rediriger vers une interface:

```
tcpdump -S -n -X -p -i eth0 -w <tracefile> --time-stamp-precision nano -tt
```

Il y a un tutoriel rapide sur `tcpdump` [ici](http://openmaniak.com/fr/tcpdump.php)  et un autre [ici](https://danielmiessler.com/study/tcpdump/) il faut vérifier que `tcpdump` supporte l'option `--time-stamp-precision nano`. L'option `-tt` permet d'afficher les timestamps unix au lieu de la date.


Normalement le ficher générer peut être traité avec `trpr` avec la commande suivante:

```
trpr input <traceFile> auto X output <plotFile>
```

Le fichier plotFile doit être traîter comme précédemment, c'est à dire diviser en deux.



### TEST 2: Test et génération de trafic FTP

Pour générer du trafic FTP, installer le serveur FTP, comme [vsftpd](http://linux.developpez.com/vsftpd/) et le configurer pour pouvoir 
effectuer des téléchargements. Vous pouvez bien sur utiliser n'importe quel 
serveur FTP.

Sur le poste client, installez Filezilla(https://filezilla-project.org/) ou tout
autre client avec lequel vous vous sentez à l'aise.


Utiliser [`tcpdump`](http://www.tcpdump.org/tcpdump_man.html)


### TEST 3: Test et génération de trafic streaming video

Pour le streaming video, utiliser l'outils [VLC](http://www.videolan.org/vlc/). Pour le streaming, utiliser [ce fichier](http://7ahiry.github.io/TedxTalk.avi). Pour lancer un streaming avec VLC sur la machine serveur, lancer VLC, File>Stream.  Chercher le fichier à diffuser. Choisissez HTTP et le port de diffusion. Choisir un mode de diffusion H323 ou autre. Diffuser.

Sur la machine cliente, file>Open network stream et renseigner les informations de diffusion adresse IP du serveur et port de diffusion. 

Utiliser [`tcpdump`](http://www.tcpdump.org/tcpdump_man.html)





